<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEnd\HomeController@index');
Route::get('/event-listing', 'FrontEnd\HomeController@eventListing');
Route::get('/event-count', 'FrontEnd\HomeController@eventCount');
Route::get('event-listing/ajax/list', 'FrontEnd\HomeController@ajaxList');
Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'BackEnd\HomeController@index');
   
    Route::prefix('event')->group(function () {
        Route::get('/', 'BackEnd\EventController@index');
        Route::post('/', 'BackEnd\EventController@store');
        Route::get('/{event}/edit', 'BackEnd\EventController@edit');
        Route::put('/{event}', 'BackEnd\EventController@update');
        Route::get('/{event}/destroy', 'BackEnd\EventController@destroy');
        Route::get('ajax/list', 'BackEnd\EventController@ajaxList');

        Route::get('/{event}/invitation', 'BackEnd\InvitationController@index');
        Route::post('/{event}/invitation', 'BackEnd\InvitationController@store');
         Route::get('/{event}/invitation/{invitation_id}/destroy', 'BackEnd\InvitationController@destroy');
        Route::get('/{event}/invitation/ajax/list', 'BackEnd\InvitationController@ajaxList');
    });

});
