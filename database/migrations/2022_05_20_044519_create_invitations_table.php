<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('creator_id')->nullable();
            $table->foreign('creator_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->unsignedInteger('updater_id')->nullable();
            $table->foreign('updater_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')
                    ->references('id')
                    ->on('events')
                    ->onDelete('no action');

            $table->string('email')->nullable();
            $table->string('status')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitations');
    }
}
