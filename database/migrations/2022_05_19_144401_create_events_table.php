<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('events', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('creator_id')->nullable();
            $table->foreign('creator_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->unsignedInteger('updater_id')->nullable();
            $table->foreign('updater_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->string('event_name');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->longText('description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
