$('.datepicker-url').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true
});

$('.datetimepicker-url').datetimepicker({
    format: "dd MM yyyy - HH:ii P",
    showMeridian: true,
    autoclose: true,
    todayBtn: true
});

$(".select2-input")
    .select2({
        placeholder: $(this).attr('placeholder')
    })
    .trigger('change');

$('.required-label').append('<span class="required-span">*</span>');
