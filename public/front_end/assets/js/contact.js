$(document).ready(function(){
    
    (function($) {
        "use strict";

       $('.datepicker-url').datepicker({
            dateFormat: 'dd-mm-yy',
            autoclose: true
        });

    jQuery.validator.addMethod(
        "validDOB",
        function(value, element) {              
            // var from = value.split(" "); // DD MM YYYY
            var from = value.split("-"); // DD/MM/YYYY

            var day = from[0];
            var month = from[1];
            var year = from[2];
            var age = 18;

            var mydate = new Date();
            mydate.setFullYear(year, month-1, day);

            var currdate = new Date();
            var setDate = new Date();

            setDate.setFullYear(mydate.getFullYear() + age, month-1, day);

            if ((currdate - setDate) > 0){
                return true;
            }else{
                return false;
            }
        },
        "Sorry, you must be 18 years of age to register"
    );


    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                first_name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                password : {
                    minlength : 6,
                },
                confirm_password : {
                    minlength : 6,
                    equalTo : "#password"
                },
                dob : {
                    validDOB : true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            },
            // messages: {
            //     email: {
            //         required: "no email, no message"
            //     },
            //     message: {
            //         required: "um...yea, you have to write something to send this form.",
            //         minlength: "thats all? really?"
            //     }
            // },
        })
    })
        
 })(jQuery)
})