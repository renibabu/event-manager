<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

namespace App;

use App\Traits\NameTrait;
use App\Traits\AvatarUrlTrait;
use Illuminate\Database\Eloquent\Model;

class Organiser extends Model
{
    use NameTrait,
        AvatarUrlTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'dob',
        'avatar',
    ];

    /**
     * The attributes that appends.
     *
     * @var array
     */
    protected $appends = [
        'avatar_url',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get the user for the admin.
     */
    public function user()
    {
        return $this->morphOne(\App\User::class, 'userable');
    }
}
