<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

namespace App\Http\Controllers\FrontEnd;

use App\Organiser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('page_name', 'Dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = \DB::table('events')
                            ->orderBy('id', 'DESC')
                            ->limit(5)
                            ->get();

        return view('front_end.home', [
            'events' => $events,
        ]);
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventListing()
    {
        return view('front_end.event');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxList(Request $request)
    {
        $events = \DB::table('events')
                            ->where(function ($query) use ($request) {
                                if ($request->filter_event) {
                                    $query->where('events.event_name', 'like', '%'.$request->filter_event.'%');
                                }
                                if ($request->filter_start_date && $request->filter_end_date) {
                                    $filter_start_date = carbonCreateDateTime('m-d-Y', $request->filter_start_date, 'Y-m-d');
                                    $filter_end_date = carbonCreateDateTime('m-d-Y', $request->filter_end_date, 'Y-m-d');
                                    $query->whereBetween('start_date', [$filter_start_date, $filter_end_date]);
                                    $query->whereBetween('end_date', [$filter_start_date, $filter_end_date]);
                                }
                                
                            })
                            ->select('*')
                            ->orderBy('events.id', 'DESC')
                            ->paginate();

        return view('front_end.event-ajax-list', [
            'events' => $events,
        ]);
    }
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventCount()
    {
        $events_count = \DB::table('events')->count();
        $organisers = Organiser::get();

        return view('front_end.event-count', [
            'events_count' => $events_count,
            'organisers' => $organisers,
        ]);
    }
}
