<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

namespace App\Http\Controllers\BackEnd;

use App\Event;
use App\Invitation;
use App\Mail\SendInvitation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class InvitationController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('page_name', 'Invitation');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event_id)
    {
        $event = Event::find($event_id);

        return view('back_end.invitation.index', [
            'event' => $event,
        ]);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invitation = Invitation::create([
            'event_id' => $request->event_id,
            'email' => $request->email,
        ]);

        Mail::to($request->email)->send(new SendInvitation($invitation));

        return response()->create($invitation, 'event/'.$request->event_id.'/invitation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id,$invitation_id)
    {
        $invitation = Invitation::find($invitation_id);

        $response = $invitation->delete();

         return response()->create($invitation, 'event/'.$event_id.'/invitation');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxList(Request $request)
    {
        $invitations = \DB::table('invitations')
                            ->where(function ($query) use ($request) {
                                if ($request->filter_event) {
                                    $query->where('events.event_name', 'like', '%'.$request->filter_event.'%');
                                }
                                if ($request->filter_email) {
                                    $query->where('invitations.email', 'like', '%'.$request->filter_email.'%');
                                }
                            })
                            ->leftJoin('events', 'invitations.event_id', '=', 'events.id')
                            ->select('invitations.id', 'invitations.email','invitations.created_at','invitations.status', 'events.event_name','invitations.event_id')
                            ->orderBy('invitations.id', 'DESC')
                            ->paginate();

        return view('back_end.invitation.ajax-list', [
            'invitations' => $invitations,
        ]);
    }

   
}