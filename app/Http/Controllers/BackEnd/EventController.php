<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

namespace App\Http\Controllers\BackEnd;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \View::share('page_name', 'Event');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back_end.event.index');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start_date = str_replace('/', '-', $request->start_date);
        $end_date = str_replace('/', '-', $request->end_date);

        $event = Event::create([
            'event_name' => $request->event_name,
            'start_date' => carbonCreateDateTime('m-d-Y', $start_date, 'Y-m-d'),
            'end_date' => carbonCreateDateTime('m-d-Y', $end_date, 'Y-m-d'),
            'description' => $request->description,
        ]);
        return response()->create($event, 'event');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id)
    {
        $event = Event::find($event_id);

        return view('back_end.event.edit', [
            'event' => $event,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $event_id)
    {
        $event = Event::find($event_id);

        $start_date = str_replace('/', '-', $request->start_date);
        $end_date = str_replace('/', '-', $request->end_date);

        $event->update([
            'event_name' => $request->event_name,
            'start_date' => carbonCreateDateTime('m-d-Y', $start_date, 'Y-m-d'),
            'end_date' => carbonCreateDateTime('m-d-Y', $end_date, 'Y-m-d'),
            'description' => $request->description,
        ]);

        return response()->update($event, 'event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        $event = Event::find($event_id);

        $response = $event->delete();

        return response()->delete($response, 'event');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxList(Request $request)
    {
        $events = \DB::table('events')
                            ->where(function ($query) use ($request) {
                                if ($request->filter_event) {
                                    $query->where('events.event_name', 'like', '%'.$request->filter_event.'%');
                                }
                                if ($request->filter_start_date) {
                                    $filter_start_date = carbonCreateDateTime('m/d/Y', $request->filter_start_date, 'Y-m-d');
                                    $query->whereDate('start_date', $filter_start_date);
                                }
                                if ($request->filter_end_date) {
                                    $filter_end_date = carbonCreateDateTime('m/d/Y', $request->filter_end_date, 'Y-m-d');
                                    $query->whereDate('end_date', $filter_end_date);
                                }
                            })
                            ->where('creator_id',auth()->user()->id)
                            ->select('*')
                            ->orderBy('events.id', 'DESC')
                            ->paginate();

        return view('back_end.event.ajax-list', [
            'events' => $events,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invitation($event_id)
    {
        $event = Event::find($event_id);

        return view('event.invitation', [
            'event' => $event,
        ]);
    }
}