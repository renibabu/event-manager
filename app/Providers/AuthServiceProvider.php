<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('privilege', function ($user, $privilege, $access_type = 'view') {
            if ($user->userable_type == 'admin') {
                return true;
            } else {
                if(!cache('user_privileges_' . $user->id)) return false;
                
                $check_privilege = cache('user_privileges_' . $user->id)->where('unique_key', $privilege)->first();

                if ($check_privilege && $check_privilege->pivot->$access_type == 'yes') return true;

                return false;
            }
        });
    }
}
