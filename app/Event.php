<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */
namespace App;

use App\Traits\CreaterTrait;
use App\Traits\ResponseMacroDestroyTrait;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use CreaterTrait,
        ResponseMacroDestroyTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name',
        'start_date',
        'end_date',
        'description',
    ];

    // /**
    //  * Get the project.
    //  */
    // public function project()
    // {
    //     return $this->belongsTo(\App\Project::class);
    // }

    // /**
    //  * Get the task.
    //  */
    // public function task()
    // {
    //     return $this->belongsTo(\App\Task::class);
    // }
}
