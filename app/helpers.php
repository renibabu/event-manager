<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

use Carbon\Carbon;

if (! function_exists('carbonCreateDateTime')) {
    function carbonCreateDateTime($format, $date, $response_format = null, $day_position = null)
    {
        if (! $response_format) {
            $response_format = 'd/m/Y H:i:s';
        }
        try {
            if ($day_position) {
                if ($day_position == 'start') {
                    return Carbon::createFromFormat($format, $date)
                            ->startOfDay()
                            ->format($response_format);
                }
                if ($day_position == 'end') {
                    return Carbon::createFromFormat($format, $date)
                            ->endOfDay()
                            ->format($response_format);
                }
            }

            return Carbon::createFromFormat($format, $date)->format($response_format);
        } catch (Exception $e) {
            return null;
        }
    }
}

if (! function_exists('createFlashMessage')) {
    function createFlashMessage($message_type, $message, $clear_previous_flash = true)
    {
        if ($clear_previous_flash) {
            session()->flash('message_type', $message_type);
            session()->flash('message', $message);
        }
    }
}

if (! function_exists('checkObject')) {
    function checkObject($collection, $objects)
    {
        if (isset($collection)) {
            $current_object = $collection;
            foreach ($objects as $key => $object) {
                $current_object = $current_object->$object;
                if (! isset($current_object)) {
                    return null;
                }
            }

            return $current_object;
        }

        return null;
    }
}

if (! function_exists('makeAvatarUrl')) {
    function makeAvatarUrl($avatar, $type = 'image')
    {
        if($avatar) return url(str_replace('public', 'storage', $avatar));

        if($type == 'image') return url('images/no-image.png');
        
        return '';
    }
}
