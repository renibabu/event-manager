<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */

namespace App\Traits;

trait NameTrait
{
    /**
     * Get the employee name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return ucwords($this->first_name.' '.($this->middle_name ? $this->middle_name.' ' : '').$this->last_name);
    }
}
