<?php

/**
 *  
 * @copyright Reni Babu
 *
 * @author Reni Babu <renibabu22@gmail.com>
 */
namespace App;

use App\Traits\CreaterTrait;
use App\Traits\ResponseMacroDestroyTrait;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use CreaterTrait,
        ResponseMacroDestroyTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id',
        'email',
        'status',
    ];

    /**
     * Get the event.
     */
    public function event()
    {
        return $this->belongsTo(\App\Event::class);
    }

    // /**
    //  * Get the task.
    //  */
    // public function task()
    // {
    //     return $this->belongsTo(\App\Task::class);
    // }
}
