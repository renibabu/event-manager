<!DOCTYPE HTML>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Evento</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{url('front_end/assets/img/favicon.ico')}}">
    <!-- CSS here -->
    <link rel="stylesheet" href="{{url('front_end/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('front_end/assets/css/custom.css')}}">
    <link href="{{url('back_end/css/toastr/toastr.min.css')}}" rel="stylesheet" />
    <link href="{{url('back_end/datepicker/jquery-ui.min.css')}}" rel='stylesheet' type='text/css'>

    @yield('style')
</head>
    <body>
        <!-- main start  -->
        <div id="main">
            @include('layouts.partials.header')
            <!-- wrapper-->
            <main>
                 @yield('content')
            </main>
            <!-- wrapper end-->
        @include('layouts.partials.footer')
          
        </div>
        <!-- Main end -->
        <!--=============== scripts  ===============-->
        <!-- JS here -->
        <script src="{{url('front_end/assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
        <!-- Jquery, Popper, Bootstrap -->
        <script src="{{url('front_end/assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{url('front_end/assets/js/popper.min.js')}}"></script>
        <script src="{{url('front_end/assets/js/bootstrap.min.js')}}"></script>
        <!-- Jquery Mobile Menu -->
        <script src="{{url('front_end/assets/js/jquery.slicknav.min.js')}}"></script>
        <script src="{{url('back_end/datepicker/jquery-ui.min.js')}}" type='text/javascript'></script>
        <!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="{{url('front_end/assets/js/owl.carousel.min.js')}}"></script>
        <script src="{{url('front_end/assets/js/slick.min.js')}}"></script>
        <!-- One Page, Animated-HeadLin -->
        <script src="{{url('front_end/assets/js/wow.min.js')}}"></script>
        <script src="{{url('front_end/assets/js/animated.headline.js')}}"></script>
        <script src="{{url('front_end/assets/js/jquery.magnific-popup.js')}}"></script>
        
        <!-- contact js -->
        <script src="{{url('front_end/assets/js/contact.js')}}"></script>
        <script src="{{url('front_end/assets/js/jquery.form.js')}}"></script>
        <script src="{{url('front_end/assets/js/jquery.validate.min.js')}}"></script>
        <script src="{{url('back_end/js/jquery-validate/additional-methods.min.js')}}"></script>
        <script src="{{url('front_end/assets/js/mail-script.js')}}"></script>
        <script src="{{url('front_end/assets/js/jquery.ajaxchimp.min.js')}}"></script>
        
        <!-- Jquery Plugins, main Jquery -->    
        <script src="{{url('front_end/assets/js/plugins.js')}}"></script>
        <script src="{{url('front_end/assets/js/main.js')}}"></script>
         <script src="{{url('back_end/js/toastr/toastr.min.js')}}"></script>
        @yield('scripts')
        
        <script type="text/javascript">
            $(function () {
                @if (Session::has('message'))
                    $(function(){
                        toastr["{{ Session::get('message_type') }}"]("{{ Session::get('message') }}")
                    });
                @endif
            });
        </script>
    </body>
</html>