<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Eventz</title>
        <link href="{{url('back_end/css/simple-datatables/style.css')}}" rel="stylesheet" />
        <!-- <link href="{{url('back_end/css/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" /> -->
        <link href="{{url('back_end/css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" />
        <link href="{{url('back_end/css/select2/select2.min.css')}}" rel="stylesheet" />
        <link href="{{url('back_end/css/toastr/toastr.min.css')}}" rel="stylesheet" />
        <link href="{{url('back_end/fontawesome-free-5.15.3-web/css/fontawesome.min.css')}}" rel="stylesheet" />
        <link href="{{url('back_end/css/styles.css')}}" rel="stylesheet" />
        <link href="{{url('back_end/css/custom.css?v=2')}}" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{url('back_end/datepicker/jquery-ui.min.css')}}" rel='stylesheet' type='text/css'>

        @yield('styles')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand ps-3" href="{{url('dashboard')}}">
                Eventz
            </a>
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" 
                id="sidebarToggle" href="#!">
                <i class="fas fa-bars"></i>
            </button>
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." 
                        aria-label="Search for..." aria-describedby="btnNavbarSearch" 
                        disabled/>
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button" 
                        disabled>
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li>
                            <a class="dropdown-item" 
                                href="{{route('logout')}}" 
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">
                                Core
                            </div>
                            <a class="nav-link" href="{{url('dashboard')}}">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-tachometer-alt"></i>
                                </div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">
                                Master
                            </div>
                            <a class="nav-link" href="{{url('event')}}">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-poll-h"></i>
                                </div>
                                Manage Events
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{auth()->user()->userable->name}}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    @yield('content')
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; ATeam Soft Solutions {{date('Y')}}</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="{{url('back_end/js/jquery-3.6.0.min.js')}}"></script>
        <script src="{{url('back_end/bootstrap-5.0.2/js/bootstrap.min.js')}}"></script>
        <script src="{{url('back_end/datepicker/jquery-ui.min.js')}}" type='text/javascript'></script>
      <!--   <script src="{{url('back_end/js/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script> -->
        <script src="{{url('back_end/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
        <script src="{{url('back_end/js/select2/select2.min.js')}}"></script>
        <script src="{{url('back_end/js/jquery-validate/jquery.validate.min.js')}}"></script>
        <script src="{{url('back_end/js/jquery-validate/additional-methods.min.js')}}"></script>
        <script src="{{url('back_end/js/toastr/toastr.min.js')}}"></script>
        <script src="{{url('back_end/js/moment/moment.min.js')}}"></script>
        <script src="{{url('back_end/fontawesome-free-5.15.3-web/js/fontawesome.min.js')}}"></script>
        <script src="{{url('back_end/js/scripts.js')}}"></script>
        <script src="{{url('back_end/js/custom.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{url('back_end/js/datatables-simple-demo.js')}}"></script>

        @yield('scripts')

        <script type="text/javascript">
            $(function () {
                @if (Session::has('message'))
                    $(function(){
                        toastr["{{ Session::get('message_type') }}"]("{{ Session::get('message') }}")
                    });
                @endif
            });
        </script>
    </body>
</html>