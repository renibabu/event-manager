 <div class="row">
  @foreach($events as $event)
    <div class="col-lg-11">
        <div class="accordion-wrapper">
            <div class="accordion" id="accordionExample">
                <!-- single-one -->
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <a href="#" class="btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$event->id}}" aria-expanded="false" aria-controls="collapse{{$event->id}}">
                                <span>{{carbonCreateDateTime('Y-m-d', $event->start_date, 'd,M-Y')}} - {{carbonCreateDateTime('Y-m-d', $event->end_date, 'd,M-Y')}}</span>
                                <h5>{{ucwords($event->event_name)}}</h5>
                            </a> 
                        </h2>
                    </div>
                    <div id="collapse{{$event->id}}" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            {{$event->description}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
      @endforeach
</div>
    {!!$events->links()!!}
    <script type="text/javascript">
    $('.pagination a').on('click', function(e){
        e.preventDefault();

        $('.ajax-list').html('<div class="text-left pl-25 pr-25"><h3 class="text-center">Please Wait...<h3></div>');

        var url = $(this).attr('href');

        var filter_event = $("#filter_event").val();
        var filter_start_date = $("#filter_start_date").val();
        var filter_end_date = $("#filter_end_date").val();
        var per_page = $("#per_page").val();
        var filter = "&filter_event=" + filter_event + "&filter_start_date=" + filter_start_date + "&filter_end_date=" + filter_end_date;

        $.get(url + filter, function(data){
            $('.ajax-list').html(data);
        });
    });
</script>