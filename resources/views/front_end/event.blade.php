@extends('layouts.front_end.front')
@section('content')
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Event Listing</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--? accordion -->
    <section class="accordion fix section-padding30">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-6">
                    <!-- Section Tittle -->
                    <div class="section-tittle text-center mb-80">
                        <h2>Event Listing</h2>
                    </div> 
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" id="filter_event" class="form-control" 
                oninput="loadAjaxList()" autocomplete="off" 
                placeholder="Event Name">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                     <input type="text" id="filter_start_date" class="form-control datepicker-url" onchange="loadAjaxList()" autocomplete="off" placeholder="Start Date">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <input type="text" id="filter_end_date" class="form-control datepicker-url" onchange="loadAjaxList()" autocomplete="off" 
                    placeholder="End Date">
                </div>
            </div>
        </div>
            <!-- Nav Card -->
            <div class="tab-content" id="nav-tabContent">
                <!-- card one -->
                <div class="tab-pane fade show active ajax-list" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab ">
                </div>
            </div>
            <!-- End Nav Card -->
        </div>
    </section>
    <!-- accordion End -->
    
@endsection
@section('scripts')
    <script type="text/javascript">
      
        function loadAjaxList() {
            $('.ajax-list').html('<div class="text-left pl-25 pr-25"><h3 class="text-center">Please Wait...<h3></div>');

            var filter_event = $("#filter_event").val();
            var filter_start_date = $("#filter_start_date").val();
            var filter_end_date = $("#filter_end_date").val();

            $.get( "/event-listing/ajax/list?filter_event=" + filter_event + "&filter_start_date=" + filter_start_date + "&filter_end_date=" + filter_end_date, function( response ) {
                $('.ajax-list').html(response);
            });
        }

        $(function () {
            loadAjaxList();
        });
    </script>
@endsection
