@extends('layouts.front_end.front')
@section('content')
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Average Count</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <section class="pricing-card-area section-padding2">
        <div class="container">
            <!-- Section Tittle -->
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8">
                    <div class="section-tittle text-center mb-100">
                        <h2>Average Events Count</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 justify-content-center">
                    <div class="single-card active text-center mb-30">
                        <div class="card-top">
                            <span>Total Events</span>
                            <h4>{{$events_count}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 justify-content-center">
                    <div class="single-card active text-center mb-30">
                        <div class="card-top">
                            <span>Total Organisers</span>
                            <h4>{{$organisers->count()}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-10 justify-content-center">
                    <div class="single-card active text-center mb-30">
                        <div class="card-top">
                            <span>Average Events Count</span>
                            @if($events_count && $organisers->count())
                                <h4>{{round($events_count/$organisers->count(),1)}}</h4>
                            @else
                                <h4>0</h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <section class="pricing-card-area section-padding2">
        <div class="container">
            <!-- Section Tittle -->
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-8">
                    <div class="section-tittle text-center mb-100">
                        <h2>Users wise Events Count</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($organisers as $organiser)

                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-8 justify-content-center">
                        <div class="single-card active text-center mb-30">
                            <div class="card-top">
                                <span>{{$organiser->name}}</span>
                                <h4>{{$organiser->user->events()->count()}}</h4>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
     
    
@endsection

