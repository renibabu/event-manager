@extends('layouts.back_end.back')
@section('content')
    <div class="container-fluid px-4">
        <h1 class="mt-1 mb-1">Event</h1>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item active">Event Master</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('event')}}" method="post" id="validate-form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-12 form-group mb-1">
                                    <label for="event_name" class="required-label">
                                       EVENT NAME
                                    </label>
                                    <input type="text" class="form-control" 
                                        id="event_name" name="event_name" placeholder="Event Name" 
                                        value="{{old('event_name')}}" autocomplete="off" 
                                        required/>
                                </div>
                                <div class="col-lg-6 form-group mb-1">
                                    <label for="start_date" class="required-label">
                                        START DATE
                                    </label>
                                    <input type="text" class="form-control datepicker-url" id="start_date" name="start_date" placeholder="Start Date" value="{{old('start_date')}}" autocomplete="off" required/>
                                </div>
                                <div class="col-lg-6 form-group mb-1">
                                    <label for="end_date" class="required-label">
                                       END DATE
                                    </label>
                                    <input type="text" class="form-control datepicker-url" id="end_date" name="end_date" placeholder="End Date" 
                                        value="{{old('end_date')}}" autocomplete="off" 
                                        required/>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12 form-group mb-1">
                                    <label for="description">
                                        DESCRIPTION
                                    </label>
                                    <textarea class="form-control" 
                                        id="description" name="description" 
                                        placeholder="Description" rows="2" 
                                        autocomplete="off">{{old('description')}}</textarea>
                                </div>
                            </div>
                            <div class="mt-4 mb-0">
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-success btn-block">
                                        ADD
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>
                                <input type="text" id="filter_event" class="form-control" 
                                    oninput="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="Event Name">
                            </th>
                            <th>
                                <input type="text" id="filter_start_date" 
                                    class="form-control datepicker-url" 
                                    onchange="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="Start Date">
                            </th>
                             <th>
                                <input type="text" id="filter_end_date" 
                                    class="form-control datepicker-url" 
                                    onchange="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="End Date">
                            </th>
                        </thead>
                    </table>
                    <div class="table-responsive ajax-list-table"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style type="text/css">
        #description {
            resize: none;
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#validate-form").validate();

        function loadAjaxListTable() {
            $('.ajax-list-table').html('<div class="text-left pl-25 pr-25"><h3 class="text-center">Please Wait...<h3></div>');

            var filter_event = $("#filter_event").val();
            var filter_start_date = $("#filter_start_date").val();
            var filter_end_date = $("#filter_end_date").val();

            $.get( "/event/ajax/list?filter_event=" + filter_event + "&filter_start_date=" + filter_start_date + "&filter_end_date=" + filter_end_date, function( response ) {
                $('.ajax-list-table').html(response);
            });
        }

        $(function () {
            loadAjaxListTable();
        });
    </script>
@endsection