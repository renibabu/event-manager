<table class="table list-table mb-0">
    <thead>
        <tr>
            <th>Event Name</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Invite</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
            <tr>
                <td>{{$event->event_name}}</td>
                <td>{{carbonCreateDateTime('Y-m-d', $event->start_date, 'd/m/Y')}}</td>
                <td>{{carbonCreateDateTime('Y-m-d', $event->end_date, 'd/m/Y')}}</td>
                <td>
                    <a href="{{url('/event/' . $event->id . '/invitation')}}">
                        <button class="btn btn-primary">Invite</button>
                    </a>
                    
                </td>
                <td>
                    <a href="{{url('/event/' . $event->id . '/edit')}}">
                        <i class="fas fa-edit text-primary"></i>
                    </a>
                    <a href="{{url('/event/' . $event->id . '/destroy')}}" 
                        onclick="return confirm('Are You Sure?')"> 
                        <i class="fas fa-trash text-danger"></i> 
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{!!$events->links()!!}

<script type="text/javascript">
    $('.pagination a').on('click', function(e){
        e.preventDefault();

        $('.ajax-list-table').html('<div class="text-left pl-25 pr-25"><h3 class="text-center">Please Wait...<h3></div>');

        var url = $(this).attr('href');

        var filter_event = $("#filter_event").val();
        var filter_start_date = $("#filter_start_date").val();
        var filter_end_date = $("#filter_end_date").val();
        var per_page = $("#per_page").val();
        var filter = "&filter_event=" + filter_event + "&filter_start_date=" + filter_start_date + "&filter_end_date=" + filter_end_date;

        $.get(url + filter, function(data){
            $('.ajax-list-table').html(data);
        });
    });
</script>