@extends('layouts.back_end.back')
@section('content')
    <div class="container-fluid px-4">
        <h1 class="mt-1 mb-1">Event Invitation</h1>
        <ol class="breadcrumb mb-1">
            <li class="breadcrumb-item active">Event Invitation Master</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('event/' . $event->id . '/invitation')}}" method="post" id="validate-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="event_id" id="event_id" value="{{$event->id}}">
                            <div class="row">
                                <div class="col-lg-12 form-group mb-1">
                                    <label for="email" class="required-label">
                                       Email
                                    </label>
                                    <input type="email" class="form-control" 
                                        id="email" name="email" placeholder="Email" 
                                        value="{{old('email')}}" autocomplete="off" 
                                        required/>
                                </div>
                            </div>
                            <hr>
                            <div class="mt-4 mb-0">
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-success btn-block"> INVITE
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>
                                <input type="text" id="filter_event" class="form-control" 
                                    oninput="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="Event Name">
                            </th>
                            <th>
                                <input type="text" id="filter_email" 
                                    class="form-control" 
                                    onchange="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="Email">
                            </th>
                             <th>
                                <input type="text" id="filter_end_date" 
                                    class="form-control datepicker-url" 
                                    onchange="loadAjaxListTable()" autocomplete="off" 
                                    placeholder="End Date">
                            </th>
                        </thead>
                    </table>
                    <div class="table-responsive ajax-list-table"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#validate-form").validate();

        function loadAjaxListTable() {
            $('.ajax-list-table').html('<div class="text-left pl-25 pr-25"><h3 class="text-center">Please Wait...<h3></div>');

            var event_id = $("#event_id").val();
            var filter_event = $("#filter_event").val();
            var filter_email = $("#filter_email").val();

            $.get("/event/"+ event_id +"/invitation/ajax/list?filter_event=" + filter_event + "&filter_email=" + filter_email, function( response ) {
                $('.ajax-list-table').html(response);
            });
        }

        $(function () {
            loadAjaxListTable();
        });
    </script>
@endsection