@extends('layouts.front_end.front')
@section('content')
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Register here</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!-- ================ contact section start ================= -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="section-heading text-center">Register Now</h2>
                    </div>
                    <div class="col-md-8 offset-md-2">
                        <form method="POST" class="form-contact contact_form" action="{{ route('register') }}" id="contactForm" novalidate="novalidate">
                            @csrf
                            <div class="row card-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="first_name" id="first_name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your first name'" placeholder="Enter your first name" required>
                                        @if ($errors->has('first_name'))
                                            <label class="error">
                                                {{ $errors->first('first_name') }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="last_name" id="last_name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your last name'" placeholder="Enter your last name" required>
                                         @if ($errors->has('last_name'))
                                            <label class="error">
                                                {{ $errors->first('last_name') }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'" placeholder="Email" required>
                                         @if ($errors->has('email'))
                                            <label class="error">
                                                {{ $errors->first('email') }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="password" id="password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your password'" placeholder="Enter your password" required>
                                         @if ($errors->has('password'))
                                            <label class="error">
                                                {{ $errors->first('password') }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" name="confirm_password" id="confirm_password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your confirm password'" placeholder="Enter your confirm password" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select name="gender" class="form-control" required>
                                            <option>--select option--</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control datepicker-url" name="dob" id="datepicker" type="text"  placeholder="Enter your date of birth" required>
                                         @if ($errors->has('dob'))
                                            <label class="error">
                                                {{ $errors->first('dob') }}
                                            </label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ contact section end ================= -->
@endsection