@extends('layouts.front_end.front')
@section('content')
        <!--? Hero Start -->
        <div class="slider-area2">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Login here</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!-- ================ contact section start ================= -->
        <section class="contact-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="section-heading text-center">Login Now</h2>
                    </div>
                    <div class="col-md-8 offset-md-2">
                        <form class="form-contact contact_form" action="{{ route('login') }}" method="post" id="validate-form" novalidate="novalidate">
                              {{ csrf_field() }}
                            <div class="row card-body">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                       <input type="text" class="form-control" placeholder="EMAIL" id="email" name="email" value="{{ old('email') }}" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                         <input type="password" class="form-control" id="password" name="password" placeholder="PASSWORD" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    
@endsection